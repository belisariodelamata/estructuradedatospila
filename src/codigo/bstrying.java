/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package codigo;

/**
 *
 * @author BS
 */
public class bstrying {
    public static String ordenarAscendentePila(Pila pila){
        Pila po=new Pila(pila.getMax());//creamos una pila del mismo tamaño
        int tope=pila.getTope();
        while(!(po.getTope()==tope)){//Mientras la pila ordenada no tenga la misma cantidad de
            //elementos de la pila original antes de ordenar
            //internamente el elemento quitado queda en la variable auxiliar de la pila
            //no es necesaria otra variabla auxiliar
            boolean eliminarElemento = pila.eliminarElemento();
            if (po.isVacia()){//si la pila ordenada esta vacia, entonces es el primer elemento
                //a colocar
                po.insertarElemento(pila.getAux());//insertamos el que sacamos de la pila
                //principal inmediatamente
            }else{//tiene algun elemento la pila ordenada
                //boolean sw=true;
                do{
                po.eliminarElemento();//quitamos un elemento de la pila ordenada
                    if (pila.getAux()<po.getAux()){//si el elemento que sacamos de la pila original
                        //es menor que el que sacamos de la pila ordenada
                        pila.insertarElemento(po.getAux());//metemos en la pila original el de la pila ordenada
                    }else{//pila original es mayor o igual que el de la pila ordenada
                        po.insertarElemento(po.getAux());//metemos el que sacamos
                        //sw=false;//y no continuamos...puedo colocar tambien un break
                break;
                    }
                }while((!(po.isVacia())));//mientras la pila no esté vacia
                //y no se haya encontrado uno mayor
                po.insertarElemento(pila.getAux());//despues que termine el ciclo de comparacion
                //el que se saco de la pila original debe meterse en la pila ordenada
                //concluí que esto siempre debe pasar....
            }
        }
        return po.getElementos();
    }


    public static boolean eliminarElemento(Pila pila, int valorelemento){
        boolean completo=false;
        if (!pila.isVacia()){//si la pila no esta vacia podemos hacer una busqueda del elemento
            Pila pilaaux=new Pila(pila.getMax());//creamos una pila auxiliar del mismo tamaño de la original
            while(!pila.isVacia()){//mientras que la pila no esté vacia va a ir comparando los valores de los elementos
                pila.eliminarElemento();
                if (pila.getAux()==valorelemento){//si encuentra el valor
            completo=true;
            break;
                }else{//coloca el elemento en la pila auxiliar
                    pilaaux.insertarElemento(pila.getAux());
                }
                
            }
            //tiene que meter todos los que depositó en la pila auxiliar
            while(!pilaaux.isVacia()){//mientras la pila auxiliar no esté vacia
                //va a sacar todos los elementos y colocarlos en la pila original
                pilaaux.eliminarElemento();
                pila.insertarElemento(pilaaux.getAux());
            }
        }
        return completo;
    }
}
