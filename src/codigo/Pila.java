
package codigo;

/**
 *
 * @author BS
 */
public class Pila {
    private int max;
    private int vector[];
    private int tope;
    private int aux;
    public Pila(int max){
        this.max=max;
        vector=new int[max];
        this.tope=-1;
    }
    public String getElementos(){
        String msg="";
        for (int i = 0; i < vector.length; i++) {
            msg+=vector[i]+"\n";
        }
        return msg;
    }
    public boolean insertarElemento(int elemento){
        if (isLlena()){
            return false;
        }else{
            tope++;
            vector[tope]=elemento;
            return true;
        }
    }
    public boolean eliminarElemento(){
        if (getTope()==-1){
            return false;
        }else{
            aux=vector[getTope()];
            tope--;
            return true;
        }
    }
    public boolean isVacia(){
        if (getTope()==-1)return true;
        else return false;
    }
    public boolean isLlena(){
        if (getTope()==this.getMax()-1)return true;
        else return false;
    }
    public int getMax() {
        return max;
    }
    public int getTope() {
        return tope;
    }
    public int getAux() {
        return aux;
    }
    
}
