/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package BsInmaginations;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;
import java.util.EventObject;

/**
 *
 * @author BS
 */
public class BsMovimiento extends Component {
    private int inc=1;
    private int c=0;
    private int xinicial=0, yinicial=0;
    private int x1=0,y1=0,x2=0,y2=0;

    private double m=0;
    private javax.swing.JComponent control=null;
    private int estado=0;
    public int getEstado(){
        if (estado==2){
            estado=0;
            return 2;
        }else{
            return estado;
        }

    }
    javax.swing.Timer t=new javax.swing.Timer(10, new ActionListener() {

        public void actionPerformed(ActionEvent e) {

            x1+=(4*inc);
            y1=(int)((double)(m*x1))+c;
            control.setBounds(x1+xinicial, y1, control.getWidth(), control.getHeight());
            if (inc>0){//si el movimiento es positivo
                if ((y2>=yinicial && y1>=y2) || (y2<=yinicial && y1<=y2)){
                    control.setBounds(control.getX(), y2, control.getWidth(), control.getHeight());
                    t.stop();
                    estado=2;
                }
            }else{
                if ((y2<=yinicial && y1<=y2) || (y2>=yinicial && y1>=y2)){
                    control.setBounds(control.getX(), y2, control.getWidth(), control.getHeight());
                    t.stop();
                    estado=2;
                }
            }
        }
    });


    public void moverTo(javax.swing.JComponent control, int x2, int y2){
        if (t.isRunning())return;
        this.estado=1;
        this.control=control;//guardo la referencia del control
        this.x1=control.getX();this.y1=control.getY();//posicion principal del control
        this.xinicial=this.x1;this.yinicial=y1;
        this.x2=x2;this.y2=y2;//destino del control
        m=((double)(y2-y1)/(double)(x2-x1));//hallo la pendiente de la linea de movimiento

        if (x2>=x1)inc=1;//compruebo el incremento que debe tener x para el movimiento
        else inc=-1;
        //if (y2>=y1)c=y1;
        //else c=y2;
        c=y1;
        x1=0;

        t.start();
    }

/////////////////////
    /////////////////
    /////////////////
// Clase para definir un nuevo tipo de evento
    class MiEvento extends EventObject {
        // Variable de instancia para diferencia a cada objeto de este tipo
        String id;
        // Constructor parametrizado
        MiEvento( Object obj,String id ) {
            // Se le pasa el objeto como parametro a la superclase
            super( obj );
            // Se guarda el identificador del objeto
            this.id = id;
            }
        // Metodo para recuperar el identificador del objeto
        String getEventoID() {
            return( id );
        }
    }
    // Define el interfaz para el nuevo tipo de receptor de eventos
    interface MiEventoListener extends EventListener {
        void destinoterminado( MiEvento evt );
    }



}
