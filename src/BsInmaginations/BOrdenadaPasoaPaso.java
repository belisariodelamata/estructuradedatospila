/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package BsInmaginations;

import codigo.Pila;

/**
 *
 * @author BS
 */
public class BOrdenadaPasoaPaso {
    private codigo.Pila pilaoriginal=null;
    private codigo.Pila pilaauxiliar=null;
    //valores devueltos por ordenarpaso a paso
    //0 nada;
    //1 colocado en el temporal original;
    //2 temporal original colocado en pila auxiliar;
    //3 colocado en el temporal auxiliar
    //4 colocado en el temporal original
    //5 comparando
    //6 colocado el temporal de auxiliar en la pila original
    //7 colocado el temporal del auxiliar en su misma pila
    //10 terminado
    private int codlastproc=0;
    private int topepilaoriginal;
    public int lastProc(){
        return codlastproc;
    }
    public BOrdenadaPasoaPaso(Pila pila){
        this.pilaoriginal=pila;
        this.pilaauxiliar=new Pila(pila.getMax());
        topepilaoriginal=pila.getTope();
    }
    public int nextProc(){
        int codproc=0;
        if (!(pilaauxiliar.getTope()==topepilaoriginal)){
            switch (codlastproc){
                case 0:case 9:
                    pilaoriginal.eliminarElemento();
                    codproc=1;
                    break;//elemento colocado en el temporal original
                case 1:
                    if (pilaauxiliar.isVacia()){
                        pilaauxiliar.insertarElemento(pilaoriginal.getAux());
                        codproc=2;
                        break;//colocado el temporal original en la pila auxiliar
                    }else{
                        pilaauxiliar.eliminarElemento();
                        codproc=7;
                        break;
                    }
                case 2:case 6:
                    if (!pilaauxiliar.isVacia()){
                          pilaauxiliar.eliminarElemento();
                          if (codlastproc==2){
                            codproc=3;//sacado elemento y colocado en el temporal del auxiliar
                          }else if (codlastproc==6){
                            codproc=11;
                          }
                    }else{
                        pilaauxiliar.insertarElemento(pilaoriginal.getAux());
                        codproc=9;
                    }
                    break;
                case 11:
                    codproc=5;
                    break;
                case 7:
                    codproc=5;
                    break;
                case 3:
                    pilaoriginal.eliminarElemento();
                    codproc=4;//sacado elemento y colocado en el temporal del original
                    break;
                case 4:
                    codproc=5;//comparando
                    break;
                case 5:
                    if (pilaoriginal.getAux()<pilaauxiliar.getAux()){
                        pilaoriginal.insertarElemento(pilaauxiliar.getAux());
                        codproc=6;//colocado el temporal del auxiliar en la pila original
                    }else{
                        pilaauxiliar.insertarElemento(pilaauxiliar.getAux());
                        codproc=8;//colocado el temporal auxiliar en su misma pila
                    }break;
                case 8:
                    pilaauxiliar.insertarElemento(pilaoriginal.getAux());
                    codproc=9;//el que saco de la pila original lo meto en la pila ordenada
                    break;
            }
        }else{
            codproc=10;//terminado
        }
        
        this.codlastproc=codproc;
        return codproc;
    }
    public static String ordenarAscendentePila(Pila pila){
        Pila po=new Pila(pila.getMax());//creamos una pila del mismo tamaño
        int tope=pila.getTope();
        while(!(po.getTope()==tope)){//Mientras la pila ordenada no tenga la misma cantidad de
            //elementos de la pila original antes de ordenar
            //internamente el elemento quitado queda en la variable auxiliar de la pila
            //no es necesaria otra variabla auxiliar
            boolean eliminarElemento = pila.eliminarElemento();
            if (po.isVacia()){//si la pila ordenada esta vacia, entonces es el primer elemento
                //a colocar
                po.insertarElemento(pila.getAux());//insertamos el que sacamos de la pila
                //principal inmediatamente
            }else{//tiene algun elemento la pila ordenada
                //boolean sw=true;
                do{
                po.eliminarElemento();//quitamos un elemento de la pila ordenada
                    if (pila.getAux()<po.getAux()){//si el elemento que sacamos de la pila original
                        //es menor que el que sacamos de la pila ordenada
                        pila.insertarElemento(po.getAux());//metemos en la pila original el de la pila ordenada
                    }else{//pila original es mayor o igual que el de la pila ordenada
                        po.insertarElemento(po.getAux());//metemos el que sacamos
                        //sw=false;//y no continuamos...puedo colocar tambien un break
                break;
                    }
                }while((!(po.isVacia())));//mientras la pila no esté vacia
                //y no se haya encontrado uno mayor
                po.insertarElemento(pila.getAux());//despues que termine el ciclo de comparacion
                //el que se saco de la pila original debe meterse en la pila ordenada
                //concluí que esto siempre debe pasar....
            }
        }
        return po.getElementos();
    }
}
