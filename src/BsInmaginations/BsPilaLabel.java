
package BsInmaginations;

/**
 *
 * @author BS
 */
public class BsPilaLabel {
    private int max;
    javax.swing.JLabel vector[];
    private int tope;
    private javax.swing.JLabel aux;
    public BsPilaLabel(int max){
        this.max=max;
        vector=new javax.swing.JLabel[max];
        this.tope=-1;
    }

    public boolean insertarElemento(javax.swing.JLabel elemento){
        if (isLlena()){
            return false;
        }else{
            tope++;
            vector[tope]=elemento;
            return true;
        }
    }
    public boolean eliminarElemento(){
        if (getTope()==-1){
            return false;
        }else{
            aux=vector[getTope()];
            tope--;
            return true;
        }
    }
    public boolean isVacia(){
        if (getTope()==-1)return true;
        else return false;
    }
    public boolean isLlena(){
        if (getTope()==this.getMax()-1)return true;
        else return false;
    }
    public int getMax() {
        return max;
    }
    public int getTope() {
        return tope;
    }
    public javax.swing.JLabel getAux() {
        return aux;
    }
    public javax.swing.JLabel getLastInsert() {
        return vector[this.getTope()];
    }
    
}
